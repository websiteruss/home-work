import { useContext } from "react";
import ProfileContext from "../context/ProfileContext";
import {Navbar} from 'react-bootstrap';
import {Link} from "react-router-dom";

function Header(){
  const {profile} = useContext(ProfileContext);

  return <header className={'bg-success d-flex'}>
  <div className={'text-white container d-flex align-items-center justify-content-between'}>
    <Navbar expand="lg">
        <Link className={'text-white navbar-brand'} to="/products"><b>Grohe</b></Link>
        <Link className={'text-white navbar-brand'} to="/about-us">О нас</Link>
        <Link className={'text-white navbar-brand'} to="/contacts">Контакты</Link>
        <Link className={'text-white navbar-brand'} to="/profile">Личный кабинет</Link>
      </Navbar>
    <div>Добро пожаловать, <b>{profile.login}</b>!</div>
  </div> 
</header>
}

export default Header;