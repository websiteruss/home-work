import { Row, Col, Form, Button } from "react-bootstrap";
import { useRef, useContext, useEffect, useState } from "react";
import ThemeContext from "../context/ThemeContext";
import {Navigate} from 'react-router-dom';


function Profile({ profile, setProfile}) {
  const [profileSaved, setProfileSaved] = useState(false);
  const {setAlertMessage} = useContext(ThemeContext);
  const loginRef = useRef(null);
  const emailRef = useRef(null);

  useEffect(() => {
    let savedProfile = localStorage.getItem('profile')
    if(savedProfile) {
      savedProfile = JSON.parse(savedProfile);
      setProfile(savedProfile);
    }
  }, [])

  function saveProfile() {
    let newProfile = {
      login: loginRef.current.value,
      email: emailRef.current.value,
    };
    setProfile(newProfile);
    localStorage.setItem('profile', JSON.stringify(newProfile))
    setAlertMessage({text: 'Добро пожаловать, ' + newProfile.login});
    loginRef.current.value = "";
    emailRef.current.value = "";
    setProfileSaved(true);
  }

    return <div className={"container"}>
        <Row>
          <Col>
            <Form>
              <h3 className={"mt-4"}>Данные пользователя</h3>
              <Form.Control
                type={"text"}
                ref={loginRef}
                placeholder={"Введите Логин"}
                className={"my-3"}
              />
              <Form.Control
                type={"email"}
                ref={emailRef}
                placeholder={"Введите email"}
                className={"my-3"}
              />
              <Button variant={"primary"} onClick={saveProfile}>
                Отправить данные
              </Button>
            </Form>
          </Col>
        </Row>
        {profileSaved ? <Navigate to = "/products" /> : ''}
      </div>
}

export default Profile;
