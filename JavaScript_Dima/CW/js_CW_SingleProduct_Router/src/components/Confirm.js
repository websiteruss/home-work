import React, {useContext, useState} from "react";
import {Button} from "react-bootstrap";
import ThemeContext from "../context/ThemeContext";
import ProfileContext from "../context/ProfileContext";

function Confirm(){
    const {theme, setTheme} = useContext(ThemeContext);
    const {profile} = useContext(ProfileContext);
    const [showConfirmation, setShowConfirmation] = useState(false);
    function showConfirm(){
        setShowConfirmation(true);
        setTimeout(() => {setShowConfirmation(false)}, 3000)
    }
    return  <>
    <Button variant={"success"} className={'my-3'} onClick={() => showConfirm()}>Confirm</Button>
    <div className={showConfirmation ? "d-block": "d-none"}>Уважаемый {profile.login}, информация по заказу отправлена на {profile.email}!</div>
    {     theme === 'bg-light' ? <Button variant={'dark'} onClick={() => setTheme('bg-dark')}>Dark</Button> :
     <Button variant={'default'} onClick={() => setTheme('bg-light')}>Light</Button>
    }
    </> 
}

export default Confirm;