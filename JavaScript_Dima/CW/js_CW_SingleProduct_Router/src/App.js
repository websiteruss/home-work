import './App.css';
import Products from './components/Products';
import ThemeContext from './context/ThemeContext';
import ProfileContext from './context/ProfileContext';
import {useState} from 'react';
import Header from './components/Header';
import Profile from './components/Profile';
import AlertMessage from './components/AlertMessage';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import AboutUs from "./components/static/AboutUs";
import Contacts from "./components/static/Contacts";
import NotFound from "./components/static/NotFound";
import SingleProduct from './components/SingleProduct';

function App() {
  const[theme, setTheme] = useState('bg-light');
  const [profile, setProfile] = useState({login: '', email: ''});
  const [alertMessage, setAlertMessage] = useState({text: ''});


  return <>
    <div className={`${theme}`} id='app'>

    <ProfileContext.Provider value={{profile, setProfile}}>
    <ThemeContext.Provider value={{ theme, setTheme, setAlertMessage}}>
    <BrowserRouter>
      <Header  />
        <Routes>
          <Route path="/about-us" element={<AboutUs />} />
          <Route path="/contacts" element={<Contacts />} />
          <Route path="/profile" element={<Profile setProfile={setProfile} profile={profile} />} />
          <Route path='/products' element={<Products/>}>
              <Route path=':productId' element={<SingleProduct/>} />
          </Route>
          <Route path="*" element={<NotFound />} />
        </Routes>
        <AlertMessage messageObj={alertMessage} />
      </BrowserRouter>
      </ThemeContext.Provider>
      </ProfileContext.Provider>
    </div>
    </>
}

export default App;
