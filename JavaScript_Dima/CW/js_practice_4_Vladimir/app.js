console.table(rates);

function renderGetRates(rates) {
    let htmlStr = '';
    for(let item in rates) {
        let index = +item + 1;
        let currency = rates[item];
        htmlStr += `<tr>
                <td>${index}</td>
                <td>${currency.r030}</td>
                <td>${currency.txt}</td>
                <td>${(currency.rate).toFixed(2)}</td>
            </tr>`;
        }
    document.querySelector('.rates tbody').innerHTML = htmlStr;
    }

    
renderGetRates(rates);
