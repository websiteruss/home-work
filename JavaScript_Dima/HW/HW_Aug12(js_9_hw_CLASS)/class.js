class Auto {
    constructor(car_model, model, year, vin) {
        this.car_model = car_model;
        this.model = model;
        this.year = year;
        this.vin = vin;
    }
    log() {
        console.log(`"${this.car_model} ${this.model} ${this.year}"`);
        return `"${this.car_model} ${this.model} ${this.year}"`;
    }
    checkVin() {
        (this.vin).length === 16 ? console.log(true) : console.log(false);
    }
}

class Auto_Fuel extends Auto {
    constructor(car_model, model, year, vin, engine_volume, consumption) {
        super(car_model, model, year, vin);
        this.car_model = car_model;
        this.model = model;
        this.year = year;
        this.vin = vin;
        this.engine_volume = engine_volume;
        this.consumption = consumption;
    }
    showFuelConsumption() {
        console.log(`"${this.engine_volume}", "${this.consumption}"`);
        return `"${this.engine_volume}", "${this.consumption}"`;
    }
}

class Auto_Electric extends Auto {
    constructor(car_model, model, year, vin, battery_capacity) {
        super(car_model, model, year, vin);
        this.car_model = car_model;
        this.model = model;
        this.year = year;
        this.vin = vin;
        this.battery_capacity = battery_capacity;
    }
    showBatteryConfig() {
        console.log(`"${this.battery_capacity}"`);
        return `"${this.battery_capacity}"`;
    }
}


