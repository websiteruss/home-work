function Auto(car_model, model, year, vin){
    this.car_model = car_model;
    this.model = model;
    this.year = year;
    this.vin = vin;
}

Auto.prototype.log = function(){
    console.log(`"${this.car_model} ${this.model} ${this.year}"`);
    return `"${this.car_model} ${this.model} ${this.year}"`;
}

Auto.prototype.checkVin = function(){

    (this.vin).length === 16 ? console.log(true) : console.log(false);
}

function Auto_Fuel(car_model, model, year, vin, engine_volume, consumption){
    this.car_model = car_model;
    this.model = model;
    this.year = year;
    this.vin = vin;
    this.engine_volume = engine_volume;
    this.consumption = consumption;
}

Auto_Fuel.prototype = Object.create(Auto.prototype);

Auto_Fuel.prototype.showFuelConsumption = function(){
    console.log(`"${this.engine_volume}", "${this.consumption}"`);
    return `"${this.engine_volume}", "${this.consumption}"`;
}

function Auto_Electric(car_model, model, year, vin, battery_capacity){
    this.car_model = car_model;
    this.model = model;
    this.year = year;
    this.vin = vin;
    this.battery_capacity = battery_capacity;
}

Auto_Electric.prototype = Object.create(Auto.prototype);

Auto_Electric.prototype.showBatteryConfig = function(){
    console.log(`"${this.battery_capacity}"`);
    return `"${this.battery_capacity}"`;
}

