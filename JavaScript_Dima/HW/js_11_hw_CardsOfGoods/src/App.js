import './App.css';
import Products from './components/Products';
import ThemeContext from './context/ThemeContext';
import {useState, useEffect} from 'react';
import Header from './components/Header';
import Profile from './components/Profile';
import Message from './components/AlertMessage';

function App() {
  const[theme, setTheme] = useState('bg-light');
  const [profile, setProfile] = useState({login: '', email: ''});
  const [alertText, setAlertText] = useState('');

  useEffect(() => {
    localStorage.setItem('Name', JSON.stringify(profile.login));
  }, [profile]);

  useEffect(() => {
    localStorage.setItem('email', JSON.stringify(profile.email));
  }, [profile]);

  return <>
    <div className={`m-0-auto ${theme} ${theme === 'bg-light' ? '' : 'text-light'}`}>

    <ThemeContext.Provider value={{ theme, setTheme, profile, alertText, setAlertText}}>
    <Header profile={profile} setProfile={setProfile}/>
      <Profile profile={profile} setProfile={setProfile} />
        <Message />
          <div className='container'>
            <Products/>
          </div>
      </ThemeContext.Provider>
    </div>
    </>
}

export default App;
