import {ListGroup, Badge, Button} from "react-bootstrap";

function CartItem({product, removeFromCart, addCount, reduceCount}) {

   
    return <ListGroup.Item className={"d-flex justify-content-between"}
            key={product.id}>{product.brand} / {product.model} ({product.price.toFixed(2)}грн)
           <div>
            <Button variant='danger' className={"mx-2 p-2 text-white cursor-pointer"} disabled={product.count === 1}
                onClick={() => reduceCount(product.id)}>-</Button>
                {product.count}         
                <Button variant='success' className={"mx-2 p-2 text-white cursor-pointer"} 
                onClick={() => addCount(product.id)}>+</Button>

                <Badge bg='danger' className={"ml-4 text-white cursor-pointer"} 
                onClick={() => removeFromCart(product.id)}>Удалить</Badge>


           </div>
    </ListGroup.Item>
}

export default CartItem;