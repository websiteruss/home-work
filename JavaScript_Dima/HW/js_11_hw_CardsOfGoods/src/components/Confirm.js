import {useContext, useState} from "react";
import {Button} from "react-bootstrap";
import ThemeContext from "../context/ThemeContext";

function Confirm(){
    const {profile, setAlertText} = useContext(ThemeContext);
    const [showConfirmation, setShowConfirmation] = useState(false);
    function showConfirm(){
        setShowConfirmation(true);
        setTimeout(() => {setShowConfirmation(false)}, 5000)
    }
    return  <>
    <Button variant="success" className={'my-3'} onClick={() => showConfirm()}>Confirm</Button>
    <div className={showConfirmation ? "d-block": "d-none"}>Уважаемый {profile.login}, информация по заказу отправлена на {profile.email}!</div>
    {setAlertText(`Товар добавлен в корзину`)}</> 
}

export default Confirm;