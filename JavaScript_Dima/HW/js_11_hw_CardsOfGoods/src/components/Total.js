import {useEffect, useState} from "react";
import Confirm from "./Confirm";

function Total({products}){
    const [total, setTotal] = useState(0);

    useEffect(() => {
        setTotal(products.reduce((acc, product) => acc + (product.price * product.count), 0))
    }, [products]);
    return<> 
    <h4 className="mt-4">Total: {total.toFixed(2)} грн</h4>
    <Confirm />
    </>
}

export default Total;