import { Row, Col, Form, Button } from "react-bootstrap";
import { useRef, useContext } from "react";
import ThemeContext from "../context/ThemeContext";

function Profile({ profile, setProfile}) {
  const {setAlertText} = useContext(ThemeContext);
  const loginRef = useRef(null);
  const emailRef = useRef(null);

  function saveProfile() {
    setProfile({
      login: loginRef.current.value,
      email: emailRef.current.value,
    });
    loginRef.current.value = "";
    emailRef.current.value = "";
    
  }
  if ((profile.login === "", profile.email === "")){
    return (
      <div className={"container"}>
        <Row>
          <Col>
            <Form>
              <h3 className={"mt-4"}>Данные пользователя</h3>
              <Form.Control
                type={"text"}
                ref={loginRef}
                placeholder={"Введите Логин"}
                className={"my-3"}
              />
              <Form.Control
                type={"email"}
                ref={emailRef}
                placeholder={"Введите email"}
                className={"my-3"}
              />
              <Button variant={"primary"} onClick={saveProfile}>
                Отправить данные
              </Button>
            </Form>
          </Col>
        </Row>
      </div>
    );
  } else {setAlertText(`Добро пожаловать, ${profile.login} !`);}
}

export default Profile;
