import {useEffect, useState, useContext} from "react";
import {Container, Row} from 'react-bootstrap';
import Product from "./Product";
import Cart from "./Cart";

function Products() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        setProducts([{ 
            id: 1,
            brand: 'Grohe',
            model: 'Смеситель для кухни Grohe Start Loop 31374000 ',
            price: 3350,
            imgSrc: 'https://cdn.27.ua/799/73/c3/291779_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 2,
            brand: 'Grohe',
            model: 'Смеситель для кухни Grohe Costa S 31067001 ',
            price: 3264,
            imgSrc: 'https://cdn.27.ua/799/15/69/529769_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 3,
            brand: 'Grohe',
            model: 'Смеситель для кухни Grohe eurosmart cosmopolitan 32843dc2',
            price: 8818,
            imgSrc: 'https://cdn.27.ua/sc--media--prod/default/ee/b5/16/eeb5162a-9a03-4459-a81d-4f27b448eb4e.webp',
            addedToCart: false,
            count: 1
        },{
            id: 4,
            brand: 'Grohe',
            model: 'Смеситель для кухни Grohe Get 31484000',
            price: 6990,
            imgSrc: 'https://cdn.27.ua/799/15/6f/529775_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 5,
            brand: 'Grohe',
            model: 'Смеситель для ванны Grohe StartEdge 23348000',
            price: 2964.80,
            imgSrc: 'https://cdn.27.ua/799/73/ec/291820_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 6,
            brand: 'Grohe',
            model: 'Смеситель для ванны Grohe Wave 32286001',
            price: 5245,
            imgSrc: 'https://cdn.27.ua/799/b5/c3/439747_6.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 7,
            brand: 'Grohe',
            model: 'Смеситель для ванны Grohe Start 32278001',
            price: 3550,
            imgSrc: 'https://cdn.27.ua/799/73/eb/291819_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 8,
            brand: 'Grohe',
            model: 'Смеситель для ванны Grohe Precision Start',
            price: 7724,
            imgSrc: 'https://cdn.27.ua/799/73/ed/291821_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 9,
            brand: 'Grohe',
            model: 'Смеситель для умывальника Grohe Wave 2015 23584001',
            price: 6500,
            imgSrc: 'https://cdn.27.ua/799/3e/73/474739_1.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 10,
            brand: 'Grohe',
            model: 'Смеситель для умывальника Grohe Wave 23585001',
            price: 6500,
            imgSrc: 'https://cdn.27.ua/799/b5/c2/439746_7.jpeg',
            addedToCart: false,
            count: 1
        },{
            id: 11,
            brand: 'Grohe',
            model: 'Смеситель для умывальника Grohe Eurosmart 30305dc1 (303051)',
            price: 6800,
            imgSrc: 'https://cdn.27.ua/sc--media--prod/default/50/4f/24/504f2481-ff6d-4116-9e44-939ebe67df2c.jpg',
            addedToCart: false,
            count: 1
        },{
            id: 12,
            brand: 'Grohe',
            model: 'Смеситель для умывальника Grohe Essence New (32898001)',
            price: 7266,
            imgSrc: 'https://cdn.27.ua/sc--media--prod/default/41/df/71/41df7151-7400-4943-b965-68b8389b0343.jpg',
            addedToCart: false,
            count: 1
        },])
    }, [])

    const addToCart = id => {
        setProducts(products.map(product => {return {...product, 
            addedToCart: product.id === id ? true : product.addedToCart}}))
    };
    const removeFromCart = id => {
        setProducts(products.map(product => {return {...product, 
            addedToCart: product.id === id ? false : product.addedToCart}}))
    };
    const addCount = id => {
        setProducts(products.map(product => {return {...product, 
            count: product.id === id ? product.count + 1 : product.count}}))
    }
    const reduceCount = id => {
        setProducts(products.map(product => {return {...product, 
            count: product.id === id ? product.count - 1 : product.count}}))
    }

    return <>
    <Container className="mx-auto">
        <Row>
            {products.map(product => <Product key={product.id} 
                     product={product} 
                     addToCart={addToCart} 
                     removeFromCart={removeFromCart} />)}
        </Row>
        {
            products.filter(el => el.addedToCart).length? 
            <Cart products={products.filter(product => product.addedToCart)}
            addCount = {addCount}
            reduceCount = {reduceCount}
            removeFromCart={removeFromCart}/> : ""
        }

    </Container>
    </>
}

export default Products;