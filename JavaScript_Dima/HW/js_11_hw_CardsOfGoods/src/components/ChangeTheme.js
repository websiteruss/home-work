import {useContext, useState} from "react";
import {Button} from "react-bootstrap";
import ThemeContext from "../context/ThemeContext";

function ChangeTheme(){
    const {theme, setTheme, profile} = useContext(ThemeContext);
    const [showConfirmation, setShowConfirmation] = useState(false);
    function showConfirm(){
        setShowConfirmation(true);
        setTimeout(() => {setShowConfirmation(false)}, 3000)
    }
    return <>
    <Button variant="success" className={'my-3'} onClick={() => showConfirm()}>Confirm</Button>
    <div className={showConfirmation ? "d-block" : "d-none"}>Hello {profile.login}, your order was sent to {profile.email}!</div>
        {
            theme === 'bg-light' ? <Button variant={'dark'} onClick={() => setTheme('bg-dark')}>Dark</Button> :
                <Button variant={'default'} onClick={() => setTheme('bg-light')}>Light</Button>
        }
    </>
}

export default ChangeTheme;