import {Container} from 'react-bootstrap';

function NotFound() {
  return <Container className={'mx-auto'}>
    <div className={'container d-flex align-items-center justify-content-between'}>
    <h2><b>Страница не найдена</b></h2>
    </div>
  </Container>
}

export default NotFound;