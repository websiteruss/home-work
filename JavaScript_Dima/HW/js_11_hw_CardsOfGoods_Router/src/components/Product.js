import { Col, Card, Button } from "react-bootstrap";
import "./Product.css";

function Product({ product, addToCart, removeFromCart }) {
  return (
    <Col xs={12} sm={6} md={4} lg={3} className={"mt-3 d-flex"}>
      <Card>
      <Card.Img className={"img"} variant="top" src={product.imgSrc} />
        <Card.Body className={"d-flex flex-column"}>
          <Card.Title className={"product-footer"}>{product.brand}</Card.Title>
          <Card.Subtitle className={"product-footer"}>
            {product.model}
          </Card.Subtitle>
          <h5 className={"my-4"}>{product.price.toFixed(2)} грн</h5>
          <div className={"product-footer"}>
            {product.addedToCart ? (
              <Button
                variant="danger"
                onClick={() => removeFromCart(product.id)}>Удалить из Корзины</Button>
            ) : (
              <Button variant="success" onClick={() => addToCart(product.id)}>Добавьте в корзину</Button>
            )}
          </div>
        </Card.Body>
      </Card>
    </Col>
  );
}

export default Product;
