import './Cart.css';
import {ListGroup} from 'react-bootstrap';
import CartItem from './CartItem';
import Total from "./Total";

function Cart({products, removeFromCart, addCount, reduceCount}){

    return <div className={'cart-block p-4 bg-white'}>
            <h3 className={"text-center"}>Корзина</h3>
            <ListGroup>
                {
                  products.map(product => <CartItem product={product} 
                                                    removeFromCart={removeFromCart}
                                                    addCount = {addCount}
                                                    reduceCount = {reduceCount}
                                                    key={product.id}/>)
                }
            </ListGroup>
            <Total products={products} />
        </div>
}

export default Cart;