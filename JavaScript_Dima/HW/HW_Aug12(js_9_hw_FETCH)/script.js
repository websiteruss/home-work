function renderProducts(products){
    console.log('Products are here', (products));

    let htmlStr = products.map(el => `<div class="card col-3">
            <div class="card-body">
            <img src="${el.images[0]}" class="card-img-top" alt="${el.title}logo">
            <h5 class="card-title">${el.category}</h5>
            <h4 class="card-title">${el.title}</h5>
            <p class="card-text">${el.description}</p>
            </div>
        </div>`).join('');
    document.getElementById('products').innerHTML = htmlStr;
}

function checkSelect(){
    document.getElementById('select').onchange = function(e){
        const value = e.currentTarget.value;
        const selectCategories = products.filter(function(el){
            return el.category === value;
        });
        renderProducts(selectCategories);
    };
};

function renderSelect(data){
    let htmlStr = `<option value = "">Не выбрано</option>`;
    htmlStr += data.map(el => `<option value="${el}">${el}</option>`).join('');
    document.getElementById('select').innerHTML = htmlStr;

    checkSelect();
}

function getProducts(){
    fetch("https://dummyjson.com/products").then(res => res.json()).then(data => {
        products = data.products;
        renderProducts(products);
    })
}

function getCategories(){
    fetch("https://dummyjson.com/products/categories").then(res => res.json()).then(data => {
        renderSelect(data);
    })
}

window.onload = () => {
    getProducts(); 
    getCategories(); 
} 