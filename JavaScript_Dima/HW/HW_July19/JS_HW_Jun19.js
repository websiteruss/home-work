console.table(countries);

function getNames(countries) {

        var countryNames = [];
        for (var item = 0; item < countries.length; item++){
            var country = countries[item];
            countryNames.push(country.name);
        }

        return countryNames;
}
var names = getNames(countries);
console.log(names);



function getCapitals(countries) {

        var countryCapitals = [];
        for (var item = 0; item < countries.length; item++){
            var capitalName = countries[item];
            countryCapitals.push(capitalName.capital);
        }
    
        return countryCapitals;
}
var capitals = getCapitals(countries);
console.log(capitals);



function getAverageArea(countries) {
        
        var averageArea = 0;
        for(var item = 0; item < countries.length; item++){
            var countryArea = countries[item];
            averageArea += +countryArea.area;
        }
        return (averageArea / countries.length).toFixed(1);
}
var average = getAverageArea(countries)
console.log(average);



function getUniqueRegions(countries) {

        var uniqueRegions = [];
        for(var item = 0; item < countries.length; item++){
            var regionUnique = countries[item];
            if (uniqueRegions.indexOf(regionUnique.region) < 0){
                uniqueRegions.push(regionUnique.region);
            }
        }
        return uniqueRegions;
}
var unique = getUniqueRegions(countries);
console.log(unique);
