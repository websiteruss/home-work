import {Col, Card, Button} from 'react-bootstrap';

function Product({product}) {
    return <Col xs={12} sm={6} md={4} lg={3} className={'my-3'}>
    <Card>
        <Card.Img variant="top" src={product.imgSrc} />
        <Card.Body>
            <Card.Title>{product.brand} {product.model}</Card.Title>
            <Card.Text>${product.price}</Card.Text>
            <Button variant="success">Add to cart</Button>
        </Card.Body>
    </Card>
    </Col>
}

export default Product;