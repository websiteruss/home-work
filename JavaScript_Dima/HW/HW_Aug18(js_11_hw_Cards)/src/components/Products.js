import { useEffect, useState } from "react";
import {Container, Row} from 'react-bootstrap';
import Product from "./Product";

function Products() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        setProducts([{ 
            id: 1,
            brand: 'Nike',
            model: 'Alpha Huarache Elite 3 Mid',
            price: 95,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/c1da532b-49f5-45b7-923f-ac2f990767da/alpha-huarache-elite-3-mid-mens-baseball-cleat-cBQ3DS.png'
        },{
            id: 2,
            brand: 'Nike',
            model: 'Lunar Cortez Baseball',
            price: 100,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,b_rgb:f5f5f5/11b40f82-f1eb-43b1-be13-053a82706707/lunar-cortez-baseball-baseball-cleats-Ld2GZ3.png'
        },{
            id: 3,
            brand: 'Nike',
            model: 'Alpha Huarache Elite 3 Low Premium',
            price: 90,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/fbd6bd54-bf39-40e4-b5fb-f12e4d9712fa/alpha-huarache-elite-3-low-premium-baseball-cleats-vX0MnF.png'
        },{
            id: 4,
            brand: 'Nike',
            model: 'Alpha Huarache Elite 3 Turf Premium',
            price: 80,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/53a8f2c2-dc64-45fc-a0fc-310443bbfe3d/alpha-huarache-elite-3-turf-premium-baseball-shoes-W1JBsP.png'
        },{
            id: 5,
            brand: 'Nike',
            model: 'Force Zoom Trout LTD Turf',
            price: 120,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/270733c4-ca0b-41ba-bda9-2f726a2660c9/force-zoom-trout-ltd-turf-mens-baseball-shoes-rtrDfG.png'
        },{
            id: 6,
            brand: 'Nike',
            model: 'Alpha Huarache Elite 3 Low',
            price: 90,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/34a9e465-0bc0-45fe-9cba-ae5e2904e94a/alpha-huarache-elite-3-low-baseball-cleat-BwGDTD.png'
        },{
            id: 7,
            brand: 'Nike',
            model: 'Alpha Huarache Elite 3 Turf',
            price: 80,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/836648da-5e7f-4a34-8d0c-833505ba3a70/alpha-huarache-elite-3-turf-baseball-shoes-CQdP8t.png'
        },{
            id: 8,
            brand: 'Nike',
            model: 'Alpha Huarache 3 Varsity Low',
            price: 43,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/4cbdb768-a0ce-41db-a9aa-1ce37437ceca/alpha-huarache-3-varsity-low-mens-baseball-cleats-ZLw5WZ.png'
        },{
            id: 9,
            brand: 'Nike',
            model: 'React Vapor Ultrafly Elite 4',
            price: 91,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/b16638d1-b14e-4d6b-bab3-e10694c52453/react-vapor-ultrafly-elite-4-mens-baseball-cleat-rBjGdm.png'
        },{
            id: 10,
            brand: 'Nike',
            model: 'Force Zoom Trout 7 Pro',
            price: 67,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/4a7edc5a-75ec-4fbe-a2a3-f19cfcf00df0/force-zoom-trout-7-pro-mens-baseball-cleats-Wpn1pM.png'
        },{
            id: 11,
            brand: 'Nike',
            model: 'Force Trout 7 Pro MCS',
            price: 70,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/7b46adf8-6d78-41a5-b1d0-e32f20e0f914/force-trout-7-pro-mcs-mens-baseball-cleats-Thr1gM.png'
        },{
            id: 12,
            brand: 'Nike',
            model: 'Force Trout 7 Keystone',
            price: 50,
            imgSrc: 'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/75908ef4-ad51-410b-9500-88d872da6e7d/force-trout-7-keystone-mens-baseball-cleats-dTD1cC.png'
        },])
    }, [])

    return <Container className="mx-auto">
        <Row>
            {products.map(product => <Product key={product.id} product={product} />)}
        </Row>
    </Container>
}

export default Products;