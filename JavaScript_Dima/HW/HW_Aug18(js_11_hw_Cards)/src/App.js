import './App.css';
import Products from './components/Products';

function App() {
  return <div className='p-4' id='app'>
    <Products/>
  </div>
}

export default App;
