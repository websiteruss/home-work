const students = [{name: 'Vasya', mark: 3.8, email: 'vasya@gmail.com', isAdmin: false},
    {name: 'Helen', mark: 3.4, email: 'helen@gmail.com', isAdmin: false},
    {name: 'Marina', mark: 4.0, email: 'marina@gmail.com', isAdmin: true},
    {name: 'Alex', mark: 4.2, email: 'alex@gmail.com', isAdmin: false},
    {name: 'Martin', mark: 4, email: 'martin@gmail.com', isAdmin: true},
    {name: 'Denys', mark: 3.7, email: 'denys@gmail.com', isAdmin: false},
    {name: 'Daniel', mark: 4.8, email: 'daniel@gmail.com', isAdmin: true},
    {name: 'John', mark: 3.6, email: 'jogn@gmail.com', isAdmin: false},
    {name: 'Phil', mark: 4.5, email: 'phil@gmail.com', isAdmin: false},
    {name: 'Anna', mark: 3.8, email: 'anna@gmail.com', isAdmin: false}];

console.log(students);

function logAdmins(students){
    //вернуть из функции список тех, у кого поле isAdmin: true

    return students.filter(function(el){
        return el.isAdmin
    })
}
let checkAdmin = logAdmins(students);
console.log(checkAdmin);


function filterByMark(students) {
    //ввести оценку через prompt
    //и вернуть из функции список тех, у кого в поле mark оценка >= чем введенная ранее

    let inputNumber = prompt("Введите оценку: ");
    return students.filter(function(el){
        return el.mark >= inputNumber
    })
}
let checkMark = filterByMark(students);
console.log(checkMark);

function renderStudents(students) {
    //по аналогии с курсами валют

    let htmlStr = '';
    for(let item in students) {
        let index = +item + 1;
        let currency = students[item];
        htmlStr += `<tr>
                <td>${index}</td>
                <td>${currency.name}</td>
                <td>${currency.mark}</td>
                <td>${currency.email}</td>
                <td>${currency.isAdmin}</td>
            </tr>`;
        }
    document.querySelector('.students tbody').innerHTML = htmlStr;
    }
renderStudents(students);

function getAverageMark(students) {
    //вернуть среднюю оценку (2 знака после запятой) из списка students
    let mark = students.map(function(el){
        return el.mark
    })

    return ((mark.reduce(function(acc, el){
        return acc + el
    }, 0)) / mark.length).toFixed(2)
}
let checkAverageMark = getAverageMark(students);
console.log(checkAverageMark);


function getEmailList(students) {
    //вернуть список состоящий из только поля email студентов
    // пример результата: ['vasya@gmail.com', 'helen@gmail.com', ..., 'anna@gmail.com']
    return students.map(function(el){
        return el.email
    })
}
let checkEmailList = getEmailList(students);
console.log(checkEmailList);

function getDataList(students) {
    //вернуть список, состоящий из name и email студентов
    //пример результата: [{name: 'Vasya', email: 'vasya@gmail.com'}, {name: 'Helen', email: 'helen@gmail.com'}, ..., {name: 'Anna', email: 'anna@gmail.com'}]
    return students.map(function(el){
        return {name: el.name,
                email: el.email
        }
    })   
}
let checkDataList = getDataList(students);
console.log(checkDataList)