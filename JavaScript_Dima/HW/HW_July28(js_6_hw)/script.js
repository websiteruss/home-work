let rates = [];

function renderRates(rates) {
    const htmlStr = rates.reduce(function(acc, el, index) {
        return acc + `<tr>
                    <td>${index + 1}</td>
                    <td>${el.name}</td>
                    <td>${el.rate}</td>
                    <td>${el.character}</td>
                    <td>${el.date}</td>
                </tr>`;
    }, '');
    document.querySelector('.table tbody').innerHTML = htmlStr;
};

document.getElementById('search').onkeyup = function(e) {
    let searchValue = e.currentTarget.value.toLowerCase().trim();
    const filteredRates = rates.filter(function(el) {
        return el.name.toLowerCase().includes(searchValue);
    });
    renderRates(filteredRates);
};

function date(){
    if (document.getElementById("myDate").value){
        return (document.getElementById("myDate").value.replaceAll('-',''));
    }else{
        let year = new Date().getFullYear().toString();
    let month = function(){
        let month = 1+(new Date().getMonth());
        if(month > 9) {return month.toString()}
        else {return ("0" + (month.toString()))};
    };
    let day = function(){
        let day = new Date().getDate();
        if(day > 9) {return day.toString()}
        else {return ("0" + (day.toString()))};
    };
        return (year+month()+day());
    };  
};
let dateNow = date();

function getRates(date){
    fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date="+`${date}`+"&json").then(function (data){ 
        return data.json();
    }).then(function(data) {
        rates = data.map(function(el) {
            return {
                name: el.txt,
                rate: el.rate,
                character: el.cc,
                date: el.exchangedate
            };
        });
        renderRates(rates);
    });
};
getRates(dateNow);

document.querySelector('.myDate_class').onchange = function(e) {
    let value = e.currentTarget.value.split('-').join('');    //let value = e.currentTarget.value.replaceAll('-','');
    if(!value) {getRates(dateNow)}
    else {getRates(value)};
    document.getElementById('search').value = '';
};

